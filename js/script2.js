var checklist = document.getElementById('checklist');
var items = checklist.querySelectorAll('li');
var inputs = checklist.querySelectorAll('input')
// console.log(checklist); // to be used to get length
// console.log(items); // to get length

// console.log(items[1]);
for (var i = 0; i < items.length; i++) {
  items[i].addEventListener('click', editItem);
  inputs[i].addEventListener('blur', updateItem);
  inputs[i].addEventListener('keypress', itemKeypress);
}


function editItem() {
  // console.log(this); // selects the clicked element with its children
  this.className = "edit"; // writes the clicked element className
  var input = this.querySelector('input') // prepares focus on input
  input.focus();
  // console.log('My current value is', input.value );
  input.setSelectionRange(0, input.value.length); //just selects the whole text range
}
function updateItem() {
  // console.log('We blurred!', this.value);
  this.previousElementSibling.innerHTML = this.value;
  this.parentNode.className="";
}
function itemKeypress(event) {
  // console.log(event);
  if (event.which == 13) {
    this.previousElementSibling.innerHTML = this.value;
    this.parentNode.className="";
  }
}














































// var checklist = document.getElementById('checklist');
//
// var items = checklist.querySelectorAll('li');
// var inputs = checklist.querySelectorAll('input')
//
// // console.log(items); // to check for li's on checlist ul
// for(var i = 0; i < items.length; i++) {
//   items[i].addEventListener('click', editItem);
//   inputs[i].addEventListener('blur', updateItem);
// }
// // loops through the whole ul (by li's) and adding an eventlistener to all
//
// function editItem() {
//   this.className = "edit";
//   var input = this.querySelector('input');
//   input.focus();
//   input.setSelectionRange(0, input.value.length);
// }
//
// function updateItem() {
//   // this.previousElementSibling.innerHTML = this.value;
//   this.previousElementSibling.innerHTML = this.value;
//   this.parentNode.className = "";
// }
